/**
 * Größe eines Bildes auf den sichtbaren Bereich optimieren
 *
 * @param   imageClass
 * @param   spareHeight
 * @since   09/2014
 * @version 09/2014
 */
function fitImageToPage(imageClass, spareHeight)
{
    var windowHeight    = $(window).height();
    var bestImageHeight = windowHeight - spareHeight;

    $(imageClass)
        .css('max-height', bestImageHeight)
        .css('visibility', 'visible');

    console.log('fitImageToPage: Resized image ' + $(imageClass).attr('src') + ' to ' + bestImageHeight + 'px')
}
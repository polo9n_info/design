$(document).ready(function() {

    Dropzone.options.pictureUpload = {

        dictDefaultMessage: "Fotos hochladen:<br>Neue Fotos einfach hier rein ziehen oder klicken",
        paramName:          "file", // The name that will be used to transfer the file
        maxFilesize:        10,     // MB

        init: function() {
            this.on("success", function(file, response) {

                location.reload();
            });
        }
    };


    // Button für weitere Uploads
    // Beim Klick wird die Dropzone sichtbar gemacht
    $('#button-upload').click(function() {

        $('#container-dropzone').toggleClass('hidden');
    });
});
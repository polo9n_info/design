$(document).ready(function() {

    // Endless-Scrolling für Thumbnails
    // http://infiniteajaxscroll.com/
    var ias = jQuery.ias({
        container:  '#thumbnails',
        item:       '.thumbnail-col',
        pagination: '.pagination',
        next:       '.next'
    });

    // Adds a loader image which is displayed during loading
    ias.extension(new IASSpinnerExtension());
});